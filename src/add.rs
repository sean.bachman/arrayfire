extern crate crossbeam_channel;
extern crate arrayfire;
use mech_core::{hash_string, TableIndex, Table, Value, ValueType, ValueMethods, Transaction, Change, TableId, Register};
use mech_utilities::{Machine, MachineRegistrar, RunLoopMessage};
//use std::sync::mpsc::{self, Sender};
use std::thread::{self};
use crossbeam_channel::Sender;
use std::collections::HashMap;
use arrayfire::{Array, Dim4, print};

lazy_static! {
  static ref ARRAYFIRE_ADD: u64 = hash_string("arrayfire/add");
  static ref LHS: u64 = hash_string("lhs");
  static ref RHS: u64 = hash_string("rhs");
  static ref RESULT: u64 = hash_string("result");
}

export_machine!(arrayfire_add, arrayfire_add_reg);

extern "C" fn arrayfire_add_reg(registrar: &mut dyn MachineRegistrar, outgoing: Sender<RunLoopMessage>) -> String {
  registrar.register_machine(Box::new(Read{outgoing}));
  "#arrayfire/add = [|lhs<f32> rhs<f32> result<f32>|]".to_string()
}

#[derive(Debug)]
pub struct Add {
  outgoing: Sender<RunLoopMessage>,
}

impl Machine for Add {

  fn name(&self) -> String {
    "arrayfire/add".to_string()
  }

  fn id(&self) -> u64 {
    Register{table_id: TableId::Global(*ARRAYFIRE_ADD), row: TableIndex::All, column: TableIndex::Alias(*LHS)}.hash() // TODO: did I get column right here?
  }

  fn on_change(&mut self, table: &Table) -> Result<(), MechError> { // TODO: What type are we returning here?
    let lhs = table.get_column(&TableIndex::Alias(*LHS), &TableIndex::Alias(*LHS));
    let rhs = table.get_column(&TableIndex::Alias(*RHS), &TableIndex::Alias(*RHS));
    match (lhs, rhs) {
      (Some((Column::F32(lhs),_)), Some((Column::F32(rhs),_))) => {
        /*
        if lhs.len() != rhs.len() {
          return Err(MechError{msg: "".to_string(), id: 6011, kind: MechErrorKind::DimensionMismatch()}); // TODO: What should I put in DimensionMismatch()?
        }
        */
        let a = lhs.clone()
        let b = rhs.clone()
        let outgoing = self.outgoing.clone();
        let add_handle = thread::spawn(move || {
          // perform math
          let lhs = Array::new(&a.unwrap(), Dim4::new(&[a.unwrap().len(),1,1,1]));
          let rhs = Array::new(&b.unwrap(), Dim4::new(&[b.unwrap().len(),1,1,1]));
          let result = arrayfire::add(&lhs, &rhs, false);
          // covert to vec::<f32>
          let mut buffer = vec!(f32::default();result.elements());
          result.host(&mut buffer);
          // create new Column::F32
          let result = Column::F32(buffer);
          outgoing.send(RunLoopMessage::Transaction(Transaction{changes: vec![
            Change::Set{table_id: *ARRAYFIRE_ADD, values: vec![(TableIndex::Alias(*RESULT), TableIndex::Alias(*RESULT), result)]}, // TODO: can I just have result at the end here?
          ]}));
        });
      }
      _ => return Err(MechError{msg: "".to_string(), id: 6012, kind: MechErrorKind::GenericError(format!("{:?}", x))});, // TODO Send error
    }
    Ok(())
  }
}